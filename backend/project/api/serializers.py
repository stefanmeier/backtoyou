from rest_framework import serializers

from project.api.models import FoundItem, LostAnimal


class FoundItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = FoundItem
        fields = ['id', 'item', 'message', 'lat', 'lng']
        read_only_fields = ['id']

    def create(self, validated_data):
        return FoundItem.objects.create(
            **validated_data
        )


class LostAnimalSerializer(serializers.ModelSerializer):
    class Meta:
        model = LostAnimal
        fields = ['id', 'created', 'animal', 'message', 'lat', 'lng', 'imagefield']
        read_only_fields = ['id']

    def create(self, validated_data):
        return LostAnimal.objects.create(
            **validated_data
        )
