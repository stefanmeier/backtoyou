# Generated by Django 2.1.2 on 2018-12-04 15:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0005_lostanimal'),
    ]

    operations = [
        migrations.AddField(
            model_name='lostanimal',
            name='imagefield',
            field=models.ImageField(null=True, upload_to='animals/', verbose_name='image'),
        ),
    ]
