from geopy import distance

from project.api.models import FoundItem, LostAnimal
from project.api.serializers import FoundItemSerializer, LostAnimalSerializer
from rest_framework.generics import ListCreateAPIView, GenericAPIView
from rest_framework.response import Response


class FoundItemView(ListCreateAPIView):
    serializer_class = FoundItemSerializer
    queryset = FoundItem.objects.all()


class LostItemView(GenericAPIView):
    serializer_class = FoundItemSerializer
    queryset = FoundItem.objects.all()

    # The following method is called POST, because it is using the request data. Actually it works as a GET.
    # To work quickly, meaning filter by distance, it would be better to use postGIS. For now a loop is ok.
    def post(self, request):
        requestedlostitemlocation = (request.data['lat'], request.data['lng'])
        found_items = []
        for i in self.queryset.all():
            if distance.distance(requestedlostitemlocation, (i.lat, i.lng)).km < 1:
                found_items.append(i)
        serializer = self.serializer_class(found_items, many=True)
        return Response(serializer.data)


class FoundAnimalView(ListCreateAPIView):
    serializer_class = LostAnimalSerializer
    queryset = LostAnimal.objects.all()

    # The following method is called POST, because it is using the request data. Actually it works as a GET.
    # To work quickly, meaning filter by distance, it would be better to use postGIS. For now a loop is ok.
    def post(self, request):
        animalocation = (request.data['lat'], request.data['lng'])
        found_animals = []
        for i in self.queryset.all():
            if distance.distance(animalocation, (i.lat, i.lng)).km < 10:
                found_animals.append(i)
        serializer = self.serializer_class(found_animals, many=True)
        return Response(serializer.data)


class LostAnimalView(ListCreateAPIView):
    serializer_class = LostAnimalSerializer
    queryset = LostAnimal.objects.all()

    def post(self, request, **kwargs):
        image = request.FILES['imagefield']
        animal = request.data['animal']
        message = request.data['message']
        lat = request.data['lat']
        lng = request.data['lng']
        instance = LostAnimal(animal=animal, message=message, lat=lat, lng=lng, imagefield=image)
        instance.save()
        return Response(LostAnimalSerializer(instance=instance).data)
