from django.db import models


# In the models is defined, how the database looks like. Each model creates a table.
# (The field 'id' is always created by django).


class FoundItem(models.Model):
    item = models.CharField(
        max_length=240,
    )
    message = models.TextField(
        max_length=2000,
        null=True
    )
    lat = models.FloatField(
        null=True
    )
    lng = models.FloatField(
        null=True
    )


class LostItem(models.Model):
    item = models.CharField(
        max_length=240,
    )
    lat = models.FloatField(
        null=True
    )
    lng = models.FloatField(
        null=True
    )


class LostAnimal(models.Model):
    created = models.DateTimeField(
        verbose_name='created',
        auto_now_add=True,
    )
    animal = models.CharField(
        max_length=240,
    )
    message = models.TextField(
        max_length=2000,
        null=True
    )
    lat = models.FloatField(
        null=True
    )
    lng = models.FloatField(
        null=True
    )
    imagefield = models.ImageField(
        verbose_name="image",
        upload_to="animals/",
        null=True
    )
