from django.contrib import admin
from .models import FoundItem, LostAnimal

admin.site.register(FoundItem)
admin.site.register(LostAnimal)
