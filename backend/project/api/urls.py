from django.urls import path

from project.api.views import FoundItemView, LostItemView, LostAnimalView, FoundAnimalView

urlpatterns = [
    path('found/', FoundItemView.as_view(), name='found-items'),
    path('lost/', LostItemView.as_view(), name='lost-items'),
    path('lostanimal/', LostAnimalView.as_view(), name='post-lost-animal'),
    path('lostanimalsearch/', FoundAnimalView.as_view(), name='search-lost-animal-announcement'),
]
