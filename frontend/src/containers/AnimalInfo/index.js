import AppBar from "../AppBar";
import React, {Component} from "react";
import connect from "react-redux/es/connect/connect";
import './index.css'


class AnimalInfo extends Component {

    render() {
        return (
            this.props.animal.message ?
                <div>
                    <AppBar
                        history={this.props.history}
                    />
                    <div className="AnimalInfo">
                        <div className="AnimalMessage">
                            You lost a {this.props.animal.animal}. That's a pity.<br/>
                            Thank you for leaving a message at <b>back to you</b>. <br/>
                            If someone looks for a lost animal 10 km next to your provided location, the following
                            message will be shown:<br/>
                        </div>
                        <div className="AnimalMessageWrapper">
                            <img className="fit-picture"
                                 src={"https://back-to-you.ch".concat(this.props.animal.imagefield)}
                                 alt={this.props.animal.imagefield}
                                 width="500"
                            />
                            <br/>
                            {this.props.animal.message}
                        </div>
                    </div>
                </div>
                :
                <div>
                    <AppBar
                        history={this.props.history}
                    />
                    <div className="AnimalInfo">
                        <div className="AnimalMessage">
                            You found a lost pet.<br/>
                            Thank you for looking for a message on <b>back to you</b>. <br/>
                            If someone lost his animal 10 km next to your provided location, this is the
                            announcement: <br/><br/>
                            {this.props.animals.map(animal =>
                                <div className="AnnouncementCard">
                                    <div key={animal.id}> Someone found a {animal.animal}.
                                        <br/>
                                        <b>Announcement:</b>
                                        <div className="AnimalMessageWrapper">
                                            {animal.created}
                                            <br/>
                                            <img className="pet-picture"
                                                 src={"https://back-to-you.ch".concat(animal.imagefield)}
                                                 alt={animal.imagefield}
                                                 width="500"
                                            />
                                            <br/>
                                            {animal.message}
                                        </div>
                                    </div>
                                </div>)
                            }
                        </div>
                    </div>
                </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        animal: state.animal,
        animals: state.animals
    }
};

export default connect(mapStateToProps)(AnimalInfo);
