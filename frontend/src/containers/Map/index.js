import React, {Component} from 'react'
import L from 'leaflet';
import Button from '../../components/Button'
import {renderToString} from 'react-dom/server'
import {connect} from 'react-redux'
import './index.css'


class Map extends Component {

    handleItem = value =>
        this.setState({item: value});

    handleMessage = value => {
        this.setState({message: value})
    };

    handleLostClick = () => {
        console.log('handleLost');
        this.props.dispatch({
            type: 'setStateToLost'
        });
    };

    handleFoundClick = () => {
        console.log('handlefound');
        this.props.dispatch({
            type: 'setStateToFound'
        })
    };

//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
    componentDidMount() {

        this.mymap = L.map('mapid', {zoomControl: false}).setView([47.36, 8.52], 10);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoibWVpZXItc3QiLCJhIjoiY2pvdTRobzBkMTQyeTNwcGg0MW1obW1oaiJ9.BhU6I7Ilz0RMpgkuFQsExA'
        }).addTo(this.mymap);

        this.popup = L.popup();

        this.mymap.on('click', this.onMapClick)
    }

    onMapClick = (e) => {
        this.popup
            .setLatLng(e.latlng)
            .setContent(
                "Did you lose or find something here?" +
                renderToString(
                    <Button
                        id="button-1"
                        requirements={this.props.location}
                        displayText={"lost"}
                    />
                ) +
                renderToString(
                    <Button
                        id="button-2"
                        requirements={this.props.location}
                        displayText={"found"}
                    />
                )
            )
            .openOn(this.mymap)
            .fire(this.props.locstate(e.latlng.lat, e.latlng.lng));
        document.getElementById('button-1').addEventListener('click', this.handleLostClick);
        document.getElementById('button-2').addEventListener('click', this.handleFoundClick)
    };

//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
    render() {

        return (
            <div>
                <div className="Map" id="mapid">
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.items
    }
};

export default connect(mapStateToProps)(Map);
