import React, {Component} from "react";
import AppBar from "../AppBar";
import './index.css';
import {connect} from 'react-redux'
import Button from "../../components/Button";

class Info extends Component {


    render() {

        return (
            this.props.lostOrFound === "Found" ?
                <div>
                    <AppBar
                        history={this.props.history}
                    />
                    <div className="Info">
                        <div className="Message">
                            You found a {this.props.foundItem.item}. <br/>
                            Thank you for leaving a hint at <b>back to you</b>. <br/>
                            If someone searches a {this.props.foundItem.item} at this location, the following message
                            wil be shown: <br/><br/>
                            <div className="MessageWrapper">
                                {this.props.foundItem.message}
                            </div>
                        </div>
                    </div>
                </div>
                :
                this.props.items.length >= 1 ?
                    <div>
                        <AppBar
                            history={this.props.history}
                        />
                        <div className="Info">
                            <div className="Message">
                                You lost a {this.props.lostItem}. <br/>
                                Thank you for searching it at <b>back to you</b>. <br/>
                                Someone found an item at this location, their message for you is shown below:
                                {this.props.items.map(item =>
                                    <div className="MessageWrapper">
                                        <div key={item.id}>
                                            Someone found a {item.item} and left the message:<br/><br/>
                                            {item.message}
                                        </div>
                                    </div>
                                )
                                }
                            </div>
                        </div>
                    </div>
                    :
                    <div>
                        <AppBar
                            history={this.props.history}
                        />
                        <div className="Info">
                            <div className="Message">
                                You lost a {this.props.lostItem}. That's a pity.<br/>
                                Thank you for searching it at <b>back to you</b>. <br/>
                                Unfortunately there was no Item found near your selected location.<br/>
                                You are free to try other locations.<br/>
                            </div>
                            <Button
                                handleClick={() => this.props.history.push('/')}
                                requirements={!null}
                                displayText={"Try other location"}

                            />
                        </div>
                    </div>
        );
    };
}


const mapStateToProps = (state) => {
    return {
        items: state.items,
        lostOrFound: state.lostOrFound,
        lostItem: state.lostItem,
        foundItem: state.foundItem
    }
};

export default connect(mapStateToProps)(Info);
