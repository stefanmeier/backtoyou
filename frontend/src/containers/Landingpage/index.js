import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css';
import AppBar from "../AppBar";
import Button from '../../components/Button';




class Landingpage extends Component {

    handleAppClick = () => {
        this.props.history.push('/home');
    };
    displayText = "weiter zur App";



    render() {
        return (
            <div className="App-Welcome">
                <AppBar
                    history={this.props.history}
                />


                <div className="Welcome">

                    <h1>
                        Welcome to «back to you».
                    </h1>
                    <p>
                        This website is for demonstration purpose only. Do not try to find your lost items or post items
                        you found.
                    </p>
                    <Button
                            handleClick={this.handleAppClick}
                            displayText={this.displayText}
                            requirements={!null}
                        />
                </div>

            </div>

        );

    }
}

export default connect()(Landingpage);
