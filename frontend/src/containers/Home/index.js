import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css';
import {addFoundItem, addLostItem} from "../../store/actions";
import Map from '../Map';
import AppBar from "../AppBar";
import Form from '../../components/Form'


class Home extends Component {
    state = {
        item: '',
        message: '',
        location: {
            lat: 80,
            lng: 0
        }
    };

    handleItem = value => {
        this.setState({item: value})
    };

    handleMessage = value => {
        this.setState({message: value})
    };

    handleFoundClick = () => {
        this.props.dispatch(addFoundItem(this.state.item, this.state.message, this.state.location.lat, this.state.location.lng));
        this.setState({
                item: '',
                message: ''
            }
        );
        this.props.history.push('/info');
    };

    handleLostClick = () => {
        this.props.dispatch(addLostItem(this.state.item, this.state.location.lat, this.state.location.lng));
        this.props.dispatch({
            type: 'addNewLostItemToStore',
            payload: this.state.item
        });
        this.setState({
                item: '',
                message: ''
            }
        );
        console.log(this.props.history);
        this.props.history.push('/info');
    };

    locstate = (lat, lng) => {
        this.setState({
            location: {
                lat: lat,
                lng: lng
            }
        })
    };

//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
    render() {
        return (
            <div className="App">
                <AppBar
                    history={this.props.history}
                />
                <div className="MapContainer">
                    <Map
                        locstate={this.locstate}
                        location={this.state.location}
                        item={this.state.item}
                    />
                    <Form
                        item={this.state.item}
                        message={this.state.message}
                        handleItem={this.handleItem}
                        handleMessage={this.handleMessage}
                        handleFoundClick={this.handleFoundClick}
                        handleLostClick={this.handleLostClick}
                        requirements={this.state.location.lat !== 80 && this.state.item}
                        displayText={"submit"}
                        lostOrFound={this.props.lostOrFound}
                    />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.items,
        lostOrFound: state.lostOrFound
    }
};

export default connect(mapStateToProps)(Home);
