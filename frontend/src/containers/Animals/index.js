import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.css';
import {addLostAnimal, searchLostAnimal} from "../../store/actions";
import AnimalMap from '../AnimalMap';
import AppBar from "../AppBar";
import Textfield from "../../components/Textfield";
import Textarea from "../../components/Textarea";
import Button from "../../components/Button";


class Animals extends Component {
    state = {
        file: null,
        animal: '',
        message: '',
        location: {
            lat: 80,
            lng: 0
        }
    };

    handleAnimal = value => {
        this.setState({animal: value})
    };

    handleMessage = value => {
        this.setState({message: value})
    };

    handleAnimalClick = () => {
        this.props.dispatch(addLostAnimal(this.state.file, this.state.animal, this.state.message, this.state.location.lat, this.state.location.lng));
        console.log('hallo');
        this.props.history.push('/animalinfo');
    };


    handleAnimalFoundClick = () => {
        this.props.dispatch(searchLostAnimal(this.state.location.lat, this.state.location.lng));
        this.props.history.push('/animalinfo');
    };

    locstate = (lat, lng) => {
        this.setState({
            location: {
                lat: lat,
                lng: lng
            }
        })
    };

    onChange = (e) => {
        this.setState({file: e.target.files[0]})
    };

//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
    render() {
        return (
            <div className="AnimalApp">
                <AppBar
                    history={this.props.history}
                />
                <div className="Body">
                    <AnimalMap
                        className="AnimalMap"
                        locstate={this.locstate}
                        location={this.state.location}
                        item={this.state.item}
                    />
                    <div className="AnimalFormWrapper">
                        <p>
                            If you lost your pet, you can publish an announcement, so people can inform you, when they
                            saw it.
                        </p>
                        <form className="AnimalForm">
                            <Textfield
                                id={"animal"}
                                type={"text"}
                                value={this.state.animal}
                                handleValue={this.handleAnimal}
                                placeholder={"animal..."}
                            />
                            <Textarea
                                id={"animalmessage"}
                                value={this.state.message}
                                handleValue={this.handleMessage}
                                placeholder={"message..."}
                            />
                            <div className="PictureButton">
                                <input
                                    className="PictureButton"
                                    type="file"
                                    onChange={this.onChange}
                                />
                            </div>
                            <div className="AnimalFormButton">
                                <Button
                                    handleClick={this.handleAnimalClick}
                                    requirements={this.state.location.lat !== 80 && this.state.animal && this.state.message}
                                    displayText={"submit announcement"}
                                />
                            </div>
                        </form>
                        <p>
                            If you found a lost pet, you can search for announcements in the area:
                        </p>
                        <div className="AnimalFormButton">
                            <Button
                                handleClick={this.handleAnimalFoundClick}
                                requirements={!null}
                                displayText={"search announcement"}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.items,
        lostOrFound: state.lostOrFound
    }
};

export default connect(mapStateToProps)(Animals);
