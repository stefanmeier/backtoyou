import React, {Component} from 'react'
import L from 'leaflet';
import {renderToString} from 'react-dom/server'
import {connect} from 'react-redux'
import './index.css'


class AnimalMap extends Component {

    handleItem = value =>
        this.setState({item: value});

    handleMessage = value => {
        this.setState({message: value})
    };

    handleLostClick = () => {
        console.log('handleLost');
        this.props.dispatch({
            type: 'setStateToLost'
        });
    };

    handleFoundClick = () => {
        console.log('handlefound');
        this.props.dispatch({
            type: 'setStateToFound'
        })
    };

//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
    componentDidMount() {

        this.mymap = L.map('mapid', {zoomControl: false}).setView([47.36, 8.52], 10);

        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoibWVpZXItc3QiLCJhIjoiY2pvdTRobzBkMTQyeTNwcGg0MW1obW1oaiJ9.BhU6I7Ilz0RMpgkuFQsExA'
        }).addTo(this.mymap);

        this.popup = L.popup();

        this.mymap.on('click', this.onMapClick)
    }

    onMapClick = (e) => {
        this.popup
            .setLatLng(e.latlng)
            .setContent("Did you last see your pet here? Then please upload a picture and provide a message.")
            .openOn(this.mymap)
            .fire(console.log(e.latlng.lat, e.latlng.lng), this.props.locstate(e.latlng.lat, e.latlng.lng));
    };

//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
    render() {

        return (

            <div className="AnimalMap" id="mapid">
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        items: state.items
    }
};

export default connect(mapStateToProps)(AnimalMap);
