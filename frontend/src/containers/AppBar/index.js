import React, {Component} from 'react'
import './index.css';
import logo from "./back_to_you_logo.svg";
import {connect} from 'react-redux';


class AppBar extends Component {

    handleLogoClick = () => {
        this.props.history.push('/');
    };

    handleAnimalsLinkClick = () => {
        this.props.history.push('/animals');
    };

    render() {
        return (
            <div className="AppBar">
                <div className="LeftSideBar">
                    <img
                        src={logo}
                        className="App-logo"
                        alt="logo"
                        onClick={this.handleLogoClick}
                    />
                    <div className="Slogan">
                        <h2>
                            where your lost items find the way back to you
                        </h2>
                    </div>
                </div>
                <div className="RightSideBar">
                    <div className="AnimalsLink" onClick={this.handleAnimalsLinkClick}>
                        Animals
                    </div>
                </div>
            </div>
        );
    };
}

export default connect()(AppBar);

