import axios from '../../axios_config'


// The getItem has the second argument getState. It is important for the functionality, even if it is not used.


export const addLostItem = (newLostItem, lat, lng) => (dispatch, getState) => {
    axios
        .post('api/lost/', {
            item: newLostItem,
            lat: lat,
            lng: lng
        })
        .then(response => {
            return response.data;
        })
        .then(MatchItem => {
            dispatch({
                type: 'addMatchItemsToStore',
                payload: MatchItem
            })
        })
};

export const addFoundItem = (newFoundItem, message, lat, lng) => (dispatch, getState) => {
    axios
        .post('api/found/', {
            item: newFoundItem,
            message: message,
            lat: lat,
            lng: lng
        })
        .then(response => {
            return response.data;
        })
        .then(FoundItem => {
            dispatch({
                type: 'addNewFoundItemToStore',
                payload: FoundItem
            })
        })
};

export const addLostAnimal = (file, animal, message, lat, lng) => (dispatch, getState) => {
    var data = new FormData();
    data.append('imagefield', file);
    data.append('animal', animal);
    data.append('message', message);
    data.append('lat', lat);
    data.append('lng', lng);
    axios
        .post('api/lostanimal/', data)
        .then(response => {
            return response.data;
        })
        .then(animal => {
            dispatch({
                type: 'addAnimalToStore',
                payload: animal
            })
        })
};

export const searchLostAnimal = (lat, lng) => (dispatch, getState) => {
    var data = new FormData();
    data.append('lat', lat);
    data.append('lng', lng);
    axios
        .post('api/lostanimalsearch/', data)
        .then(response => {
            return response.data;
        })
        .then(animals => {
            dispatch({
                type: 'addMatchAnimalsToStore',
                payload: animals
            })
        })
};