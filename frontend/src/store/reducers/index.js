import {applyMiddleware, createStore} from 'redux'
import thunk from 'redux-thunk';

// the initial state is empty, we can see that the app is connected by checking the logged props for dispatch ()
const initialState = {lostItem: [], foundItem: [], animal: [], items: [], animals: [], location: [], lostOrFound: ""};


// the reducer copies the state, changes things and returns the new one.
// the type of the actions decide what gets changed in the state.
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'addNewLostItemToStore': {
            return {...state, lostItem: action.payload};
        }
        case 'addMatchItemsToStore': {
            const items = state.items.map(item => item);
            return {...state, items: items.concat(action.payload)};
        }
        case 'addNewFoundItemToStore': {
            return {...state, foundItem: action.payload};
        }
        case 'addLocationToStore': {
            return {...state, location: action.payload}
        }
        case 'setStateToLost': {
            return {...state, lostOrFound: "Lost"}
        }
        case 'setStateToFound': {
            return {...state, lostOrFound: "Found"}
        }
        case 'addAnimalToStore': {
            return {...state, animal: action.payload}
        }
        case 'addMatchAnimalsToStore': {
            return {...state, animals: action.payload};
        }
        default:
            return state
    }
};

const store = createStore(
    reducer,
    applyMiddleware(thunk)
);

export default store
