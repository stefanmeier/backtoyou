import React from 'react'
import Textfield from "../Textfield";
import Button from '../Button'
import './index.css'

const Form = ({item, message, handleItem, handleMessage, handleFoundClick, handleLostClick, requirements, displayText, lostOrFound}) => {

    return (lostOrFound === "Found" ?
            <div className="Form">
                <form className="Form">
                    <Textfield
                        id={"item"}
                        type={"text"}
                        value={item}
                        handleValue={handleItem}
                        placeholder={"item..."}
                    />
                    <Textfield
                        id={"message"}
                        type={"text"}
                        value={message}
                        handleValue={handleMessage}
                        placeholder={"message..."}
                        tooltip={"Please leave a message to tell where you brought the found item. You can also leave your e-mail address."}
                    />
                    <Button
                        handleClick={handleFoundClick}
                        requirements={requirements && message}
                        displayText={displayText}
                        tooltip={"Please click on the map, where you found the item. And fill in the Textfield on the left what you found."}
                    />
                </form>
            </div>
            :
            lostOrFound === "Lost" ?
                <div className="Form">
                    <form className="Form">
                        <Textfield
                            id={"item"}
                            type={"text"}
                            value={item}
                            handleValue={handleItem}
                            placeholder={"item..."}
                            keypress={() => handleLostClick}
                        />
                        <Button
                            handleClick={handleLostClick}
                            requirements={requirements}
                            displayText={displayText}
                            tooltip={"Please click on the map, where you lost the item approximately. And fill in the Textfield on the left what you lost."}
                        />
                    </form>
                </div>
                :
                <div className="Form">
                    <Button
                        handleClick={handleLostClick}
                        requirements={requirements}
                        displayText={displayText}
                        tooltip={"Please click on the map, where you lost or found the item."}
                    />
                </div>
    );
};

export default Form;
