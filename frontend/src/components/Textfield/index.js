import React, {Component} from 'react'
import './index.css';


class Textfield extends Component {

    render() {

        return (
            <div className="TextFieldContainer" tooltip={this.props.tooltip} tooltip-position="top">
                <input
                    className="Textfield"
                    style={{height: '25px'}}
                    id={this.props.id}
                    type={this.props.type}
                    value={this.props.value}
                    onChange={(event) => this.props.handleValue(event.target.value)}
                    placeholder={this.props.placeholder}
                />

            </div>
        );
    }
}

export default Textfield;
