import React from 'react'
import './index.css';

const Button = ({id, handleClick, requirements, displayText, tooltip}) => {

    const onClick = () => {
        console.log('click!');
        handleClick()
    };

    return (
        <div className="Container">
            <button
                id={id}
                className="Button"
                onClick={onClick}
                disabled={!requirements}
                tooltip={tooltip}
                tooltip-position="top"
            >
                {displayText}
            </button>
        </div>
    );
};

export default Button;
