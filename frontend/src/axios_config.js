import axios from 'axios';

let baseAPIUrl;
if (process.env.NODE_ENV === 'development') {
    baseAPIUrl = 'http://localhost:8000/backend/';
} else {
    baseAPIUrl = 'https://back-to-you.ch/backend/';
}

const instance = axios.create({
    baseURL: baseAPIUrl
});

export default instance;
