import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Landingpage from './containers/Landingpage';
import Home from './containers/Home';
import Info from './containers/Info';
import Animals from './containers/Animals';
import AnimalInfo from './containers/AnimalInfo';
import * as serviceWorker from './serviceWorker';
import store from './store/reducers'
import {Provider} from 'react-redux'
import {BrowserRouter, Route, Switch} from "react-router-dom";

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={Landingpage}/>
                <Route exact path="/home" component={Home}/>
                <Route exact path="/info" component={Info}/>
                <Route exact path="/animals" component={Animals}/>
                <Route exact path="/animalinfo" component={AnimalInfo}/>
            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
