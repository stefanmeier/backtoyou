## back to you  
  
This is my final project at Propulsion academy.

 - Frontend: React app with a leaflet map and Redux.
 - Backend: Django rest framework with postgres database.
 - Deployment: Docker through gitlab pipelie and an Nginx webserver.

### Purpose of the App:

 - Items: If you find a lost item on the street or in a building its not allowed to keep it. 
You have to bring it to the owner of the house or somwhere else. With this app, 
you can leave a message for the one that is looking for it. 

 - Animals: When your pet ran away, you can leave a picture and a message that 
other people can help you looking for it.

### Funcionality:

 - looking for lost items at a certain location.
 - posting a message for a found item
 
 - posting an announcement for your lost pet
 - looking for lost animals (10km)
 

### Outlook (still to do):

 - Using PostGIS as database to work with geodata. (faster and possibility to 
 post routes.)
 - User Registration for people who are looking for lost items.
 - Using elasticsearch or similar to provide a better user experience. (When the 
 user lost his hat, and someone left a message for a cap, it should be shown. 
 But the user does not need to know if someone lost a laptop nearby.)
  - Catching errors and add tests.